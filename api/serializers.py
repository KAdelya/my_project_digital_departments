from rest_framework import serializers

from web.forms import User
from web.models import AdditionalInformation


class UserSerializer(serializers.Serializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class AdditionalInformationSerializer(serializers.Serializer):
    user = UserSerializer(read_only=True)

    def save(self, **kwargs):
        return super().save(**kwargs)

    class Meta:
        model = AdditionalInformation
        fields = ('id', 'age', 'sex', 'lastname', 'user')
