from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class AdditionalInformation(models.Model):
    lst = (
        ('мужской', 'мужской'),
        ('женский', 'женский')
    )
    age = models.IntegerField()
    sex = models.CharField(max_length=20, choices=lst)
    lastname = models.CharField(max_length=20)
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.lastname
    class Meta:
        verbose_name = 'дополнительная информация'
        verbose_name_plural = 'дополнительные информации'
