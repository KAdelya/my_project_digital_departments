from django.contrib import admin

from web.models import AdditionalInformation


class AdditionalInformationAdmin(admin.ModelAdmin):
    list_display = ('age', 'sex', 'lastname', 'user')
    search_fields = ('age', 'sex', 'lastname')
    list_filter = ('age', 'sex', 'lastname')
    ordering = ('lastname',)


admin.site.register(AdditionalInformation, AdditionalInformationAdmin)
