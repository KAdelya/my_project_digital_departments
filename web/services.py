import csv

from agestatistic.redis import get_redis_client


def filter_additional_information(additional_information_qs, filters: dict):
    if filters["search"]:
        additional_information_qs = additional_information_qs.filter(lastname__icontains=filters["search"])
    if filters["search_age"]:
        additional_information_qs = additional_information_qs.filter(age__icontains=filters["search_age"])
    if filters["search_sex"]:
        additional_information_qs = additional_information_qs.filter(sex__icontains=filters["search_sex"])
    return additional_information_qs


def export_additional_information(additional_information_qs, response):
    writer = csv.writer(response)
    writer.writerow(("age", "sex", "lastname"))
    for additional_information in additional_information_qs:
        writer.writerow((additional_information.age, additional_information.sex, additional_information.lastname))
    return response


def get_stat():
    redis = get_redis_client()
    keys = redis.keys("stat_*")
    return [(key.decode().replace("stat_", ""), redis.get(key).decode()) for key in keys]
