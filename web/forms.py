from django import forms
from django.contrib.auth import get_user_model

from web.models import AdditionalInformation

User = get_user_model()


class RegistrationForm(forms.ModelForm):
    password2 = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data["password"] != cleaned_data["password2"]:
            self.add_error("password", "Пароли не совпадают")
        return cleaned_data

    class Meta:
        model = User
        fields = ("email", "username", "password", "password2")


class AuthForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class AdditionalInformationForm(forms.ModelForm):
    def save(self, commit=True):
        self.instance.user = self.initial["user"]
        return super().save(commit)

    class Meta:
        model = AdditionalInformation
        fields = ("age", "sex", "lastname")


class AdditionalInformationFilterForm(forms.Form):
    search = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Отчество"}), required=False)
    search_age = forms.IntegerField(widget=forms.TextInput(attrs={"placeholder": "Возраст"}), required=False)
    search_sex = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Пол"}), required=False)
