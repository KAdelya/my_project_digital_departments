from django.urls import path

from web.views import main_view, registration_view, auth_view, logout_view, additional_information_add_or_edit_view, \
    additional_information_delete_view, analytics_view, stat_view

urlpatterns = [
    path("", main_view, name="main"),
    path("analytics/", analytics_view, name="analytics"),
    path("stat/", stat_view, name="stat"),
    path("registration/", registration_view, name="registration"),
    path("auth/", auth_view, name="auth"),
    path("logout/", logout_view, name="logout"),
    path("additional_information/add_or_edit/", additional_information_add_or_edit_view,
         name="additional_information_add_or_edit"),
    path("additional_information/<int:id>/delete", additional_information_delete_view,
         name="additional_information_delete")
]
