from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page

from web.forms import RegistrationForm, AuthForm, AdditionalInformationForm, AdditionalInformationFilterForm
from web.models import AdditionalInformation
from django.core.paginator import Paginator
from django.db import connection

from web.services import filter_additional_information, export_additional_information, get_stat

User = get_user_model()


@cache_page(2)
def main_view(request):
    additional_information = AdditionalInformation.objects.all().order_by('lastname')
    additional_information = additional_information.select_related("user")
    filter_form = AdditionalInformationFilterForm(request.GET)
    filter_form.is_valid()
    additional_information = filter_additional_information(additional_information, filter_form.cleaned_data)
    page_number = request.GET.get("page", 1)
    paginator = Paginator(additional_information, per_page=1000)
    total_count = additional_information.count()
    if request.GET.get("export") == "csv":
        response = HttpResponse(
            content_type="text/csv",
            headers={"Content-Disposition": 'attachment; filename="additional_information.csv"'}
        )
        return export_additional_information(additional_information, response)
    with connection.cursor() as cursor:
        cursor.execute("SELECT MAX(age), MIN(age), COUNT(id) FROM web_additionalinformation")
        info = cursor.fetchone()
        max_age = info[0]
        min_age = info[1]
        count_users_with_additional_info = info[2]
    return render(request, "web/main.html", {
        "additional_information": paginator.get_page(page_number),
        "total_count": total_count,
        "max_age": max_age,
        "min_age": min_age,
        "count_users_with_additional_info": count_users_with_additional_info,
        "filter_form": filter_form
    })


@login_required
def analytics_view(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT MAX(age), MIN(age), COUNT(id) FROM web_additionalinformation")
        info = cursor.fetchone()
        max_age = info[0]
        min_age = info[1]
        count_users_with_additional_info = info[2]
    return render(request, "web/analytics.html", {
        "max_age": max_age,
        "min_age": min_age,
        "count_users_with_additional_info": count_users_with_additional_info
    })


def registration_view(request):
    is_success = False
    form = RegistrationForm()
    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = User(username=form.cleaned_data["username"],
                        email=form.cleaned_data["email"])
            user.set_password(form.cleaned_data['password'])
            user.save()
            is_success = True
    return render(request, "web/registration.html", {"form": form, "is_success": is_success})


def auth_view(request):
    form = AuthForm()
    if request.method == "POST":
        form = AuthForm(data=request.POST)
        if form.is_valid():
            user = authenticate(**form.cleaned_data)
            if user is None:
                form.add_error(None, "Введены неверные данные")
            else:
                login(request, user)
                return redirect("main")
    return render(request, "web/auth.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("main")


@login_required
def additional_information_add_or_edit_view(request):
    form = AdditionalInformationForm()
    if request.method == "POST":
        form = AdditionalInformationForm(data=request.POST, initial={"user": request.user})
        if form.is_valid():
            form.save()
            return redirect("main")
    return render(request, "web/additional_information_form.html", {"form": form})


@login_required
def additional_information_delete_view(request, id):
    additional_information = AdditionalInformation.objects.get(user_id=id)
    additional_information.delete()
    return redirect("main")


@login_required
def stat_view(request):
    return render(request, "web/stat.html", {
        "results": get_stat()
    })
