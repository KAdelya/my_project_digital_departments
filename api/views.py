from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from api.serializers import AdditionalInformationSerializer
from web.models import AdditionalInformation


@api_view(["GET"])
@permission_classes([])
def main_view(request):
    return Response({"status": "ok"})


class AdditionalInformationModelViewSet(ModelViewSet):
    serializer_class = AdditionalInformationSerializer

    def get_queryset(self):
        return AdditionalInformation.objects.all().select_related("user").filter(user=self.request.user)


